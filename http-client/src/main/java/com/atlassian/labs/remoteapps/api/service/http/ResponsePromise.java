package com.atlassian.labs.remoteapps.api.service.http;

/**
 * A specific type of BaseResponsePromise for handling a response promise
 * for a single HTTP request
 */
public interface ResponsePromise extends BaseResponsePromise<Response>
{
}
