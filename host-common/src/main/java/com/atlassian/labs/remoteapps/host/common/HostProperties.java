package com.atlassian.labs.remoteapps.host.common;

/**
 *
 */
public interface HostProperties
{
    String getKey();
}
