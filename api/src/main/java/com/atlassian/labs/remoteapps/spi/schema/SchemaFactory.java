package com.atlassian.labs.remoteapps.spi.schema;

/**
 * Creates schema instances
 */
public interface SchemaFactory
{
    Schema getSchema();
}
