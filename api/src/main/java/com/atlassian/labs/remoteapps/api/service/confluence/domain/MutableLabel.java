package com.atlassian.labs.remoteapps.api.service.confluence.domain;

/**
 */
public interface MutableLabel
{
    void setId(long id);

    long getId();
}
