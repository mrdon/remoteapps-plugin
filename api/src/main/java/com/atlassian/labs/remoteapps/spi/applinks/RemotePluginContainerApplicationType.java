package com.atlassian.labs.remoteapps.spi.applinks;

import com.atlassian.applinks.spi.application.NonAppLinksApplicationType;

/**
 * Application type for all remote plugin container links
 */
public interface RemotePluginContainerApplicationType extends NonAppLinksApplicationType
{
}
